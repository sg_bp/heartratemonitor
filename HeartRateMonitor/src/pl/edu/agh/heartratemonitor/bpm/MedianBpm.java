package pl.edu.agh.heartratemonitor.bpm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MedianBpm implements BpmStrategy {

	BpmStrategy strategy;
	int n;

	public MedianBpm(BpmStrategy strategy, int n) {
		this.strategy = strategy;
		this.n = n;
	}

	@Override
	public Integer computeBpm(List<Measurement> m) {
		measureNew(m);
		return median();
	}

	List<Integer> results = new LinkedList<Integer>();

	private void measureNew(List<Measurement> m) {
		Integer result = strategy.computeBpm(m);
		if (result == null)
			return;
		results.add(result);
		while (results.size() > n)
			results.remove(0);
	}

	private Integer median() {
		if (results.isEmpty())
			return null;
		List<Integer> sorted = new ArrayList<Integer>(results);
		Collections.sort(sorted);
		return sorted.get(sorted.size() / 2);
	}

}
