package pl.edu.agh.heartratemonitor.bpm;

public class Measurement {
	public long time;
	public double value;
	
	public Measurement(long time, double value) {
		this.time = time;
		this.value = value;
	}
}
