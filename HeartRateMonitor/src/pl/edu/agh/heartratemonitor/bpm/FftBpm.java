package pl.edu.agh.heartratemonitor.bpm;

import java.util.List;

import pl.edu.agh.heartratemonitor.lib.FFT;

public class FftBpm implements BpmStrategy {

	private static final double THRESHOLD_FREQ = 40;

	int k;

	public FftBpm(int resolution) {
		k = resolution;
	}

	private double[] compute(List<Measurement> m) {
		double[] re = probe(m), im = new double[k];
		new FFT(k).fft(re, im);
		for (int i = 0; i < k; ++i)
			re[i] = abs(re[i], im[i]);
		return re;
	}

	private static double abs(double re, double im) {
		return Math.sqrt(re * re + im * im);
	}

	private static double midpoint(double time, Measurement a, Measurement b) {
		double alfa = (time - a.time) / (b.time - a.time);
		return a.value + alfa * (b.value - a.value);
	}

	public double[] probe(List<Measurement> m) {
		int n = m.size();
		double[] probes = new double[k];
		if (n < 2)
			return probes;
		probes[0] = m.get(0).value;
		double unit_time = (m.get(n - 1).time - m.get(0).time) / (k - 1);
		int j = 1;
		for (int i = 1; i < k - 1; ++i) {
			double time = m.get(0).time + i * unit_time;
			while (j < n - 1 && m.get(j).time < time - 1)
				++j;
			probes[i] = midpoint(time, m.get(j - 1), m.get(j));
		}
		probes[k - 1] = m.get(n - 1).value;
		return probes;
	}

	@Override
	public Integer computeBpm(List<Measurement> m) {
		lastComputedAmplitudes = compute(m);
		int n = m.size();
		double time_m = (m.get(n - 1).time - m.get(0).time) / (double) 1000
				/ 60;
		double maxAmp = 0, f = 0;
		for (int i = 1; i < n / 2; ++i)
			if (lastComputedAmplitudes[i] > maxAmp || f < THRESHOLD_FREQ) {
				maxAmp = lastComputedAmplitudes[i];
				f = i / time_m;
			}
		return (int) Math.round(f);
	}

	public double[] lastComputedAmplitudes = new double[] {};
}
