package pl.edu.agh.heartratemonitor.bpm;

import java.util.List;

public class AvgBpm implements BpmStrategy {

	private static final int WINDOW_LIMIT = 40;
	
	@Override
	public Integer computeBpm(List<Measurement> measurements) {
		if (measurements.isEmpty())
			return null;
		measurements = measurements.subList(
				Math.max(0, measurements.size() - WINDOW_LIMIT), measurements.size());

		double avg = average(measurements);
		int beats = 0;
		double start = measurements.get(measurements.size() - 1).time, end = 0;
		boolean above = measurements.get(0).value >= avg;
		for (Measurement m : measurements) {
			if (m.value < avg) {
				if (above) {
					beats++;
					start = Math.min(start, m.time);
					end = Math.max(end, m.time);
				}
				above = false;
			} else
				above = true;
		}

		double bpm = (beats - 1) / (end - start) * 1000 * 60;
		return (int) Math.round(bpm);
	}

	private static double average(List<Measurement> measurements) {
		double sum = 0;
		for (Measurement m : measurements)
			sum += m.value;
		return sum / measurements.size();
	}
}