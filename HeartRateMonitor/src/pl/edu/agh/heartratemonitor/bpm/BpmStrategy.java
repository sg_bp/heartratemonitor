package pl.edu.agh.heartratemonitor.bpm;

import java.util.List;

public interface BpmStrategy {

	Integer computeBpm(List<Measurement> m);

}
