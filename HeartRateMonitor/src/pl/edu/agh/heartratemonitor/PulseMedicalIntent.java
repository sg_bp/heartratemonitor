package pl.edu.agh.heartratemonitor;

import android.content.Intent;

public class PulseMedicalIntent extends Intent {
	public static final String LOINC_CODE = "8867-4";

	public PulseMedicalIntent(int pulse) {
		setAction("meddev.MEASUREMENT");
		putExtra("LOINC_LIST", new String[] { LOINC_CODE });
		putExtra("LOINC_VALUES", new String[] { String.valueOf(pulse) });
	}
}
