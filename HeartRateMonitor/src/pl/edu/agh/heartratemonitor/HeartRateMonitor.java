package pl.edu.agh.heartratemonitor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import pl.edu.agh.heartratemonitor.bpm.AvgBpm;
import pl.edu.agh.heartratemonitor.bpm.BpmStrategy;
import pl.edu.agh.heartratemonitor.bpm.FftBpm;
import pl.edu.agh.heartratemonitor.bpm.Measurement;
import pl.edu.agh.heartratemonitor.bpm.MedianBpm;
import pl.edu.agh.heartratemonitor.lib.ImageProcessing;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjoe64.graphview.BarGraphView;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;


public class HeartRateMonitor extends Activity {

	private static final int MEDIAN_ELEMENTS = 41;
    public static final String TAG = "HeartRateMonitor";

    private SurfaceHolder previewHolder;
    private Camera camera;
    private TextView text;

    private WakeLock wakeLock;
    
    private GraphViewSeries exampleSeries1, exampleSeries2;
    private GraphView graphView, graphView2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        createGraph();
        createAmpGraph();
        
        SurfaceView preview = (SurfaceView) findViewById(R.id.preview);
        previewHolder = preview.getHolder();
        previewHolder.addCallback(surfaceCallback);

        text = (TextView) findViewById(R.id.text);
        findViewById(R.id.okButton).setOnClickListener(buttonOnClickListener);

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");
    }
    
    private OnClickListener buttonOnClickListener = new OnClickListener(){
		@Override
		public void onClick(View v) {
			sendBroadcast(new PulseMedicalIntent(fftBpm)); 
		}
	};

    private GraphViewData[] prepareHeartRateRawData(){
		List<GraphViewData> result = new ArrayList<GraphViewData>();
		double[] p = fft.probe(measurements);
	    for(int i=0; i<p.length; ++i)
	    	result.add(new GraphViewData(i, p[i]));
	    	//result.add(new GraphViewData(m.time, m.value));
    	return result.toArray(new GraphViewData[] {});
    }
    
    private GraphViewData[] prepareAmplitudesData() {
    	List<GraphViewData> result = new ArrayList<GraphViewData>();
    	double[] amps = fft.lastComputedAmplitudes;
	    for(int i=1; i<amps.length/2; ++i)
	    	result.add(new GraphViewData(i, amps[i]));
    	return result.toArray(new GraphViewData[] {});
    }
    
    private void createAmpGraph(){
    	// init example series data
        exampleSeries2 = new GraphViewSeries(prepareAmplitudesData());
        exampleSeries2.getStyle().color = Color.BLUE;

        graphView2 = new BarGraphView(
                this, // context
                (String)null // heading
        );
        
        graphView2.addSeries(exampleSeries2); // data
        graphView2.setHorizontalLabels(new String[0]);
        graphView2.getGraphViewStyle().setVerticalLabelsWidth(1);
        graphView2.getGraphViewStyle().setNumVerticalLabels(0);
        graphView2.setVerticalLabels(new String[0]);

        LinearLayout layout = (LinearLayout) findViewById(R.id.amplitudes_graph);
        layout.addView(graphView2);
    }
    
    private void createGraph(){
    	// init example series data
        exampleSeries1 = new GraphViewSeries(prepareHeartRateRawData());
        exampleSeries1.getStyle().color = Color.RED;

        graphView = new LineGraphView(
                this, // context
                (String)null // heading
        );
        
        graphView.addSeries(exampleSeries1); // data
        graphView.setHorizontalLabels(new String[] {});
        graphView.getGraphViewStyle().setNumVerticalLabels(3);

        LinearLayout layout = (LinearLayout) findViewById(R.id.heart_graph);
        layout.addView(graphView);
    }

    
    private Runnable mTimer;
    private final Handler mHandler = new Handler();
    
    @Override
    public void onResume() {
        super.onResume();
        final int interval = 50;
        
        wakeLock.acquire();

        camera = Camera.open();

        // graph timers
        mTimer = new Runnable() {
            @Override
            public void run() {
                exampleSeries1.resetData(prepareHeartRateRawData());
                exampleSeries2.resetData(prepareAmplitudesData());
                mHandler.postDelayed(this, interval);
            }
        };
        mHandler.postDelayed(mTimer, interval);
    }

    @Override
    public void onPause() {
        super.onPause();

        wakeLock.release();

        camera.setPreviewCallback(null);
        camera.stopPreview();
        camera.release();
        camera = null;
        
        mHandler.removeCallbacks(mTimer);
    }

    private BpmStrategy bpsStrategy = new MedianBpm(new AvgBpm(), MEDIAN_ELEMENTS);
    private FftBpm fft = new FftBpm(FFT_RESOLUTION);
    private BpmStrategy medianFft = new MedianBpm(fft, MEDIAN_ELEMENTS);
    private Integer fftBpm;
    
    private static final int WINDOW_SIZE_MILLIS = 10 * 1000;
    private static final int FFT_RESOLUTION = 256;
    private final List<Measurement> measurements = new LinkedList<Measurement>();
    
    private void handleMeasurement(Measurement m) {
    	measurements.add(m);
    	long window_start = System.currentTimeMillis() - WINDOW_SIZE_MILLIS;
        while(!measurements.isEmpty() && measurements.get(0).time < window_start)
        	measurements.remove(0);
    	
    	Integer bpm = bpsStrategy.computeBpm(measurements);
    	fftBpm = medianFft.computeBpm(measurements);
        text.setText(s(bpm) + " " + s(fftBpm));
    }
    
    private static String s(Integer i) {
    	return i!=null? i.toString() : "--";
    }
    
    private PreviewCallback previewCallback = new PreviewCallback() {
		@Override
		public void onPreviewFrame(byte[] data, Camera camera) {
			Camera.Size size = camera.getParameters().getPreviewSize();
	        double value = ImageProcessing.decodeYUV420SPtoRedAvg(data.clone(), size.height, size.width);
	        handleMeasurement(new Measurement(System.currentTimeMillis(), value));
		}
    };

    private SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(previewHolder);
                camera.setPreviewCallback(previewCallback);
            } catch (Throwable t) {
                Log.e("PreviewDemo-surfaceCallback", "Exception in setPreviewDisplay()", t);
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            Camera.Size size = getSmallestPreviewSize(width, height, parameters);
            if (size != null) {
                parameters.setPreviewSize(size.width, size.height);
                Log.d(TAG, "Using width=" + size.width + " height=" + size.height);
            }
            camera.setParameters(parameters);
            camera.startPreview();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // Ignore
        }
    };

    private static Camera.Size getSmallestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea < resultArea) result = size;
                }
            }
        }

        return result;
    }
}


