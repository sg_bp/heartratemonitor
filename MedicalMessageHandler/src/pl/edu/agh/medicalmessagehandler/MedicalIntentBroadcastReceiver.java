package pl.edu.agh.medicalmessagehandler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.widget.Toast;

public class MedicalIntentBroadcastReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		String pulse = intent.getStringArrayExtra("LOINC_VALUES")[0];
		Toast.makeText(context, "Pulse intent received: " + pulse, Toast.LENGTH_LONG)
			.show();
		// Vibrate the mobile phone
		Vibrator vibrator = (Vibrator) context
				.getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(2000);
	}

} 
