package pl.edu.agh.medicalmessagehandler;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity{
	
	private Button testButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		this.testButton = (Button) findViewById(R.id.testButton);
		this.testButton.setOnClickListener(testButtonOnClickListener);

	}
	
	private OnClickListener testButtonOnClickListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			intent.setAction("meddev.MEASUREMENT");
			sendBroadcast(intent); 
		}
		
	};
}
